(function () {
    'use strict';

    var app = angular.module('GanttChartDemo', [
        'gantt',
        'gantt.sortable',
        'gantt.movable',
        'gantt.drawtask',
        'gantt.tooltips',
        'gantt.bounds',
        'gantt.progress',
        'gantt.table',
        'gantt.tree',
        'gantt.groups',
        'gantt.resizeSensor',
        'gantt.overlap',
        'gantt.dependencies'
    ]);

    app.controller('Ctrl', [
        '$scope',
        function ($scope) {
            $scope.ganttOptions = {
              fromDate:new Date(2017, 8, 5, 10, 0, 0),
              toDate:new Date(2017, 8, 10, 11, 30, 0),
              viewScale: "30 minutes",
            };
            $scope.tooltipTemplate = '<dl class="dl-horizontal"><dt>Name</dt><dd>{{task.model.name}}</dd><dt>Actor</' +
                    'dt><dd>Reporting Member State</dd><dt>Due date</dt><dd>{{task.model.to| date: ' +
                    '' +
                    '"dd/MM/yyyy"}}</dd></dl>';
            $scope.timeFrames = {
                closed: {
                    color: 'gray',
                    magnet: false, // This will disable magnet snapping
                    working: false // We don't work when it's closed
                },
                submissionDate: {
                    color: 'purple',
                    magnet: false,
                    working: false
                },
                clockStop: {
                    color: 'red',
                    magnet: false, // This will disable magnet snapping
                    working: false // We don't work when it's closed
                }
            };
            $scope.headersFormats = {
                day: 'MMMM DD',
                hour: 'HH:mm',
                minute: 'mm',
                halfHour: 'mm'
            };
            $scope.dateFrames = {
                submission: {
                    date: moment('2017-09-05', 'YYYY-MM-DD'), // A specific date
                    targets: ['submissionDate'] // Use timeFrame named day for halloween. We won't close for noon.
                },
                clockStop: {
                    start: moment('2017-09-15', 'YYYY-MM-DD'), // A date range
                    end: moment('2017-09-17', 'YYYY-MM-DD'),
                    targets: ['clockStop'] // use timeFrame named closed for this date range.
                },
                weekend: {
                    evaluator: function (date) { // A custom function evaluated for each day in the gantt
                        return date.isoWeekday() === 6 || date.isoWeekday() === 7;
                    },
                    targets: ['closed'] // Use timeFrame named closed for saturday and sunday.
                }
            };
            $scope.getColumnWidth = function (widthEnabled, scale, zoom) {
                if (!widthEnabled && $scope.canAutoWidth(scale)) {
                    return undefined;
                }

                if (scale.match(/.*?week.*?/)) {
                    return 150 * zoom;
                }

                if (scale.match(/.*?month.*?/)) {
                    return 300 * zoom;
                }

                if (scale.match(/.*?quarter.*?/)) {
                    return 500 * zoom;
                }

                if (scale.match(/.*?year.*?/)) {
                    return 800 * zoom;
                }

                return 40 * zoom;
            }
            $scope.data = [
                {
                    name: 'Driver 1'
                }, {
                    name: 'Task 1',
                    parent: 'Driver 1',
                    tasks: [
                        {
                            name: '台北 - 台北',
                            color: '#9FC5F8',
                            from: new Date(2017, 8, 5, 11, 30, 0),
                            to: new Date(2017, 8, 5, 13, 30, 0)
                        }
                    ]
                }, {
                    name: 'Task 2',
                    parent: 'Driver 1',
                    tasks: [
                        {
                            name: '桃園 - 桃園',
                            color: '#5cb85c',
                            priority: 10,
                            from: new Date(2017, 8, 5, 13, 30, 0),
                            to: new Date(2017, 8, 5, 15, 30, 0)
                        }
                    ]
                }, {
                    name: 'Task 3',
                    parent: 'Driver 1',
                    tasks: [
                        {
                            name: '新竹 - 台中',
                            color: '#5cb85c',
                            from: new Date(2017, 8, 5, 16, 30, 0),
                            to: new Date(2017, 8, 5, 20, 30, 0)
                        }
                    ]
                },{
                    name: 'Driver 2'
                }, {
                    name: 'Task 1',
                    parent: 'Driver 2',
                    tasks: [
                        {
                            name: '台北 - 台中',
                            color: '#9FC5F8',
                            from: new Date(2017, 8, 5, 10, 30, 0),
                            to: new Date(2017, 8, 5, 14, 30, 0)
                        }
                    ]
                }, {
                    name: 'Task 2',
                    parent: 'Driver 2',
                    tasks: [
                        {
                            name: '台中 - 苗栗',
                            color: '#5cb85c',
                            priority: 10,
                            from: new Date(2017, 8, 5, 16, 30, 0),
                            to: new Date(2017, 8, 5, 18, 30, 0)
                        }
                    ]
                }, {
                    name: 'Task 3',
                    parent: 'Driver 2',
                    tasks: [
                        {
                            name: '苗栗 - 新竹',
                            color: '#5cb85c',
                            from: new Date(2017, 8, 5, 20, 30, 0),
                            to: new Date(2017, 8, 5, 23, 30, 0)
                        }
                    ]
                }
            ];
        }
    ]);
})();